import React, { Component } from 'react';
import TodoList from '../components/TodoList';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as todosActions from '../store/modules/todos';

class TodoListContainer extends Component {
  getTodoList = () => {
      const {TodosActions} = this.props;
      TodosActions.getTodoList();
      //console.error(this.props);
  }

  componentDidMount() {
    this.getTodoList();
  }

  handleToggle = (ID, DONE) => {
    const { TodosActions } = this.props;
    
    if (DONE){
      DONE = false;
    }
    else
    {
      DONE = true;
    }
    const todo = {
      id: ID,
      done: DONE
    };

    TodosActions.updateTodo(todo);
    TodosActions.getTodoList();
  }
  handleRemove = (id) => {
    const { TodosActions } = this.props;
    //TodosActions.remove(id);
    TodosActions.removeTodo(id);
    TodosActions.getTodoList();
  }
  render() {
    const { todos, loading } = this.props;
    const { handleToggle, handleRemove } = this;
    if(loading) return null;
    //console.error(todos);
    return (
      <TodoList 
        todos={todos}
        onToggle={handleToggle}
        onRemove={handleRemove}
      />
    );
  }
}

export default connect(
  (state) => ({
    todos: state.todos.get('todo')
  }),
  (dispatch) => ({
    TodosActions: bindActionCreators(todosActions, dispatch)
  })
)(TodoListContainer)